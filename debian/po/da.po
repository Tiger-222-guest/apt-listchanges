# Danish translation for apt-listchanges.
# Copyright (C) 2011 apt-listchanges og nedenstående oversættere.
# This file is distributed under the same license as the apt-listchanges package.
# Claus Hindsgaul <claus.hindsgaul@gmail.com>, 2004, 2006.
# Joe Hansen (joedalton2@yahoo.dk), 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: da\n"
"Report-Msgid-Bugs-To: apt-listchanges@packages.debian.org\n"
"POT-Creation-Date: 2017-11-12 23:48+0100\n"
"PO-Revision-Date: 2011-01-10 19:25+0200\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#. Type: select
#. Choices
#: ../templates:2001
msgid "pager"
msgstr "tekstviser"

#. Type: select
#. Choices
#: ../templates:2001
msgid "browser"
msgstr "browser"

#. Type: select
#. Choices
#: ../templates:2001
msgid "xterm-pager"
msgstr "xterm-tekstviser"

#. Type: select
#. Choices
#: ../templates:2001
msgid "xterm-browser"
msgstr "xterm-browser"

#. Type: select
#. Choices
#: ../templates:2001
msgid "gtk"
msgstr "gtk"

#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../templates:2001 ../templates:4001
msgid "text"
msgstr "tekst"

#. Type: select
#. Choices
#: ../templates:2001
msgid "mail"
msgstr "post"

#. Type: select
#. Choices
#: ../templates:2001
msgid "none"
msgstr "ingen"

#. Type: select
#. Description
#: ../templates:2002
msgid "Method to be used to display changes:"
msgstr "Metode der skal bruges til visning af ændringer:"

#. Type: select
#. Description
#: ../templates:2002
msgid ""
"Changes in packages can be displayed in various ways by apt-listchanges:"
msgstr "Ændringer i pakker kan vises på forskellige måder af apt-listchanges:"

#. Type: select
#. Description
#: ../templates:2002
msgid ""
" pager        : display changes one page at a time;\n"
" browser      : display HTML-formatted changes using a web browser;\n"
" xterm-pager  : like pager, but in an xterm in the background;\n"
" xterm-browser: like browser, but in an xterm in the background;\n"
" gtk          : display changes in a GTK window;\n"
" text         : print changes to the terminal (without pausing);\n"
" mail         : only send changes via e-mail;\n"
" none         : do not run automatically from APT."
msgstr ""
" tekstviser      : benyt din foretrukne tekstviser til at vise ændringerne\n"
"                   en side ad gangen;\n"
" browser         : vis HTML-formaterede ændringer med en internetbrowser;\n"
" xterm-tekstviser: som tekstviser, men i en xterm i baggrunden;\n"
" xterm-browser   : som browser, men i en xterm i baggrunden;\n"
" gtk             : vis ændringer i et GTK-vindue;\n"
" text            : skriv ændringerne til din terminal (uden at holde\n"
"                   pauser);\n"
" post            : send kun ændringerne med e-post;\n"
" ingen           : undlad at køre automatisk fra apt."

#. Type: select
#. Description
#: ../templates:2002
msgid ""
"This setting can be overridden at execution time. By default, all the "
"options except for 'none' will also send copies by mail."
msgstr ""
"Denne indstilling kan tilsidesættes ved udførelsestid. Som standard vil alle "
"indstillinger undtagen »ingen« (none) også sende kopier med post."

#. Type: string
#. Description
#: ../templates:3001
msgid "E-mail address(es) which will receive changes:"
msgstr "E-post-adresser der skal modtage ændringerne:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Optionally, apt-listchanges can e-mail a copy of displayed changes to a "
"specified address."
msgstr ""
"apt-listchanges kan valgfrit sende en kopi af de viste ændringer til en "
"angiven adresse."

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Multiple addresses may be specified, delimited by commas. Leaving this field "
"empty disables mail notifications."
msgstr ""
"Der kan angives flere adresser adskilt af komma. Hvis dette felt er tomt, "
"vil alle e-post-påmindelser være deaktiveret."

#. Type: select
#. Choices
#: ../templates:4001
msgid "html"
msgstr ""

#. Type: select
#. Description
#: ../templates:4002
msgid "Format of e-mail messages:"
msgstr ""

#. Type: select
#. Description
#: ../templates:4002
#, fuzzy
#| msgid "Please choose which type of changes should be displayed with APT."
msgid ""
"Please choose a format for e-mail copies of the displayed changes - either "
"plain text or HTML with clickable links."
msgstr "Vælg Hvilke typer ændringer, der skal vises med APT."

#. Type: boolean
#. Description
#: ../templates:5001
msgid "Prompt for confirmation after displaying changes?"
msgstr "Skal der bedes om bekræftelse efter visning af ændringer?"

#. Type: boolean
#. Description
#: ../templates:5001
msgid ""
"After displaying the list of changes, apt-listchanges can pause with a "
"confirmation prompt. This is useful when running from APT, as it offers an "
"opportunity to abort the upgrade if a change is unwelcome."
msgstr ""
"Når apt-listchanges har givet dig mulighed for at se listen over ændringer, "
"kan den spørge, om du vil fortsætte. Dette er nyttigt, når den køres fra "
"APT, da det giver dig mulighed for at afbryde opgraderingen, hvis denne ikke "
"er velkommen (på dette tidspunkt)."

#. Type: boolean
#. Description
#: ../templates:5001
msgid ""
"This can be overridden at execution time, and has no effect if the "
"configured frontend option is 'mail' or 'none'."
msgstr ""
"Dette kan tilsidesættes på udførelsestidspunktet, og har ingen effekt hvis "
"den konfigurerede grænsefladeindstilling er »post« eller »ingen«."

#. Type: boolean
#. Description
#: ../templates:6001
#, fuzzy
#| msgid "Should apt-listchanges skip changes that have already been seen?"
msgid "Insert headers before changelogs?"
msgstr ""
"Skal apt-listchanges springe de ændringer over, der er blevet vist før?"

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"apt-listchanges can insert a header before each package's changelog showing "
"the name of the package, and the names of the binary packages which are "
"being upgraded (when different from the source package name)."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"Note however that displaying headers might make the output a bit harder to "
"read as they might contain long lists of names of binary packages."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:7001
msgid "Disable retrieving changes over network?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"In rare cases when a binary package does not contain a changelog file, apt-"
"listchanges by default executes the command \"apt-get changelog\", which "
"tries to download changelog entries from the network."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"This option can disable this behavior, which might for example be useful for "
"systems with limited network connectivity."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Show changes in reverse order?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"By default apt-listchanges shows changes for each package in the order of "
"their appearance in the relevant changelog or news files - from the most "
"recent version of the package to the oldest."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Optionally apt-listchanges can display changes in the opposite order, which "
"some may find more natural: from the oldest changes in the package to the "
"newest."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:9001
msgid "Should apt-listchanges skip changes that have already been seen?"
msgstr ""
"Skal apt-listchanges springe de ændringer over, der er blevet vist før?"

#. Type: boolean
#. Description
#: ../templates:9001
msgid ""
"A record of already displayed changes can be kept in order to avoid "
"displaying them again. This is useful, for example, when retrying an upgrade."
msgstr ""
"apt-listchanges kan holde styr på hvilke ændringer, der allerede er blevet "
"vist. Dette er nyttigt, hvis du f.eks. gentager et forsøg på at opgradere."

#. Type: select
#. Choices
#: ../templates:10001
msgid "news"
msgstr "nyheder"

#. Type: select
#. Choices
#: ../templates:10001
msgid "changelogs"
msgstr "ændringslogger (changelogs)"

#. Type: select
#. Choices
#: ../templates:10001
msgid "both"
msgstr "begge"

#. Type: select
#. Description
#: ../templates:10002
msgid "Changes displayed with APT:"
msgstr "Ændringer vist med APT:"

#. Type: select
#. Description
#: ../templates:10002
msgid "Please choose which type of changes should be displayed with APT."
msgstr "Vælg Hvilke typer ændringer, der skal vises med APT."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
" news      : important news items only;\n"
" changelogs: detailed changelogs only;\n"
" both      : news and changelogs."
msgstr ""
" nyheder: Kun vigtige nyheder\n"
" ændringslogger (changelogs): Kun detaljerede ændringslogger\n"
" begge: Nyheder og ændringslogger (changelogs)."
