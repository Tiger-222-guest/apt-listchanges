# translation of apt-listchanges_2.82_sv.po to Swedish
#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
# Martin Ågren <martin.agren@gmail.com>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: apt-listchanges_2.82_sv\n"
"Report-Msgid-Bugs-To: apt-listchanges@packages.debian.org\n"
"POT-Creation-Date: 2017-11-12 23:48+0100\n"
"PO-Revision-Date: 2008-07-23 19:54+0200\n"
"Last-Translator: Martin Ågren <martin.agren@gmail.com>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#. Type: select
#. Choices
#: ../templates:2001
msgid "pager"
msgstr "visare"

#. Type: select
#. Choices
#: ../templates:2001
msgid "browser"
msgstr "webbläsare"

#. Type: select
#. Choices
#: ../templates:2001
msgid "xterm-pager"
msgstr "xterm-visare"

#. Type: select
#. Choices
#: ../templates:2001
msgid "xterm-browser"
msgstr "xterm-bläddrare"

#. Type: select
#. Choices
#: ../templates:2001
msgid "gtk"
msgstr "gtk"

#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../templates:2001 ../templates:4001
msgid "text"
msgstr "text"

#. Type: select
#. Choices
#: ../templates:2001
msgid "mail"
msgstr "e-post"

#. Type: select
#. Choices
#: ../templates:2001
msgid "none"
msgstr "ingen"

#. Type: select
#. Description
#: ../templates:2002
msgid "Method to be used to display changes:"
msgstr "Metod för att visa ändringar:"

#. Type: select
#. Description
#: ../templates:2002
msgid ""
"Changes in packages can be displayed in various ways by apt-listchanges:"
msgstr "Ändringar i paket kan visas på olika sätt av apt-listchanges:"

#. Type: select
#. Description
#: ../templates:2002
msgid ""
" pager        : display changes one page at a time;\n"
" browser      : display HTML-formatted changes using a web browser;\n"
" xterm-pager  : like pager, but in an xterm in the background;\n"
" xterm-browser: like browser, but in an xterm in the background;\n"
" gtk          : display changes in a GTK window;\n"
" text         : print changes to the terminal (without pausing);\n"
" mail         : only send changes via e-mail;\n"
" none         : do not run automatically from APT."
msgstr ""
" visare          : visa ändringar en sida åt gången;\n"
" webbläsare      : visa HTML-formaterade ändringar med en webbläsare;\n"
" xterm-visare    : som visare men i en xterm-terminal i bakgrunden;\n"
" xterm-bläddrare : som bläddrare men i en xterm-terminal i bakgrunden;\n"
" gtk             : visa ändringar i ett GTK-fönster;\n"
" text            : skriv ut ändringar till din terminal (utan paus);\n"
" e-post          : skicka endast ändringar via e-post;\n"
" ingen           : kör inte automatiskt från APT."

#. Type: select
#. Description
#: ../templates:2002
msgid ""
"This setting can be overridden at execution time. By default, all the "
"options except for 'none' will also send copies by mail."
msgstr ""
"Denna inställning kan ignoreras vid körning. Som standard sänder alla val "
"förutom \"ingen\" även en kopia via e-post."

#. Type: string
#. Description
#: ../templates:3001
msgid "E-mail address(es) which will receive changes:"
msgstr "E-postadress(er) som ska ta emot ändringar:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Optionally, apt-listchanges can e-mail a copy of displayed changes to a "
"specified address."
msgstr "apt-listchanges kan skicka en kopia av visade ändringar."

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Multiple addresses may be specified, delimited by commas. Leaving this field "
"empty disables mail notifications."
msgstr ""
"Flera adresser kan anges, avgränsade med kommatecken.  Lämna detta fält "
"blankt om du inte vill att några e-postmeddelanden ska skickas."

#. Type: select
#. Choices
#: ../templates:4001
msgid "html"
msgstr ""

#. Type: select
#. Description
#: ../templates:4002
msgid "Format of e-mail messages:"
msgstr ""

#. Type: select
#. Description
#: ../templates:4002
#, fuzzy
#| msgid "Please choose which type of changes should be displayed with APT."
msgid ""
"Please choose a format for e-mail copies of the displayed changes - either "
"plain text or HTML with clickable links."
msgstr "Välj vilka typer av ändringar som ska visas med APT."

#. Type: boolean
#. Description
#: ../templates:5001
msgid "Prompt for confirmation after displaying changes?"
msgstr "Fråga efter bekräftelse efter visning av ändringar?"

#. Type: boolean
#. Description
#: ../templates:5001
msgid ""
"After displaying the list of changes, apt-listchanges can pause with a "
"confirmation prompt. This is useful when running from APT, as it offers an "
"opportunity to abort the upgrade if a change is unwelcome."
msgstr ""
"Efter att ha visat listan över ändringar kan apt-listchanges fråga dig om du "
"vill fortsätta eller inte. Detta är användbart när den körs från APT "
"eftersom det ger dig en chans att avbryta uppgradering om du ser en ändring "
"som du inte vill genomföra (ännu)."

#. Type: boolean
#. Description
#: ../templates:5001
msgid ""
"This can be overridden at execution time, and has no effect if the "
"configured frontend option is 'mail' or 'none'."
msgstr ""
"Denna inställning kan ignoreras vid körning och har ingen påverkan om den "
"konfigurerade visaren är \"e-post\" eller \"ingen\"."

#. Type: boolean
#. Description
#: ../templates:6001
#, fuzzy
#| msgid "Should apt-listchanges skip changes that have already been seen?"
msgid "Insert headers before changelogs?"
msgstr "Ska apt-listchanges hoppa över ändringar som redan har visats?"

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"apt-listchanges can insert a header before each package's changelog showing "
"the name of the package, and the names of the binary packages which are "
"being upgraded (when different from the source package name)."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:6001
msgid ""
"Note however that displaying headers might make the output a bit harder to "
"read as they might contain long lists of names of binary packages."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:7001
msgid "Disable retrieving changes over network?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"In rare cases when a binary package does not contain a changelog file, apt-"
"listchanges by default executes the command \"apt-get changelog\", which "
"tries to download changelog entries from the network."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"This option can disable this behavior, which might for example be useful for "
"systems with limited network connectivity."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Show changes in reverse order?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"By default apt-listchanges shows changes for each package in the order of "
"their appearance in the relevant changelog or news files - from the most "
"recent version of the package to the oldest."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Optionally apt-listchanges can display changes in the opposite order, which "
"some may find more natural: from the oldest changes in the package to the "
"newest."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:9001
msgid "Should apt-listchanges skip changes that have already been seen?"
msgstr "Ska apt-listchanges hoppa över ändringar som redan har visats?"

#. Type: boolean
#. Description
#: ../templates:9001
msgid ""
"A record of already displayed changes can be kept in order to avoid "
"displaying them again. This is useful, for example, when retrying an upgrade."
msgstr ""
"apt-listchanges har möjlighet att hålla kontroll på vilka ändringar som "
"redan har visats och undvika att visa dem igen. Detta är användbart, till "
"exempel, när en uppgradering upprepas."

#. Type: select
#. Choices
#: ../templates:10001
msgid "news"
msgstr "nyheter"

#. Type: select
#. Choices
#: ../templates:10001
msgid "changelogs"
msgstr "ändringsloggar"

#. Type: select
#. Choices
#: ../templates:10001
msgid "both"
msgstr "båda"

#. Type: select
#. Description
#: ../templates:10002
msgid "Changes displayed with APT:"
msgstr "Ändringar visade med APT:"

#. Type: select
#. Description
#: ../templates:10002
msgid "Please choose which type of changes should be displayed with APT."
msgstr "Välj vilka typer av ändringar som ska visas med APT."

#. Type: select
#. Description
#: ../templates:10002
msgid ""
" news      : important news items only;\n"
" changelogs: detailed changelogs only;\n"
" both      : news and changelogs."
msgstr ""
" nyheter       : endast viktiga nyhetsposter\n"
" ändringsloggar: endast detaljerade ändringsloggar\n"
" båda          : både nyheter och detaljerade ändringsloggar."
