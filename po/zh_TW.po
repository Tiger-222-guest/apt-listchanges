# Traditional Chinese translation to apt-listchanges
# Kanru Chen <koster@debian.org.tw>, 2005
#
msgid ""
msgstr ""
"Project-Id-Version: apt-listchanges\n"
"Report-Msgid-Bugs-To: apt-listchanges@packages.debian.org\n"
"POT-Creation-Date: 2017-07-08 22:48+0200\n"
"PO-Revision-Date: 2008-10-03 22:01+0800\n"
"Last-Translator: Kanru Chen <koster@debian.org.tw>\n"
"Language-Team: Chinese/Traditional <zh-l10n@linux.org.tw>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.4\n"

#: ../apt-listchanges.py:62
#, python-format
msgid "Unknown frontend: %s"
msgstr "未知的介面：%s"

#: ../apt-listchanges.py:79
#, python-format
msgid "Cannot reopen /dev/tty for stdin: %s"
msgstr ""

#: ../apt-listchanges.py:125
#, python-format
msgid "News for %s"
msgstr "%s 的消息"

#: ../apt-listchanges.py:127
#, python-format
msgid "Changes for %s"
msgstr "%s 的改變"

#: ../apt-listchanges.py:130
msgid "Informational notes"
msgstr "訊息備忘"

#: ../apt-listchanges.py:137
msgid "apt-listchanges: News"
msgstr "apt-listchanges: 消息"

#: ../apt-listchanges.py:138
msgid "apt-listchanges: Changelogs"
msgstr "apt-listchanges: 改變紀錄"

#: ../apt-listchanges.py:144
#, python-format
msgid "apt-listchanges: news for %s"
msgstr "apt-listchanges: %s 的消息"

#: ../apt-listchanges.py:145
#, python-format
msgid "apt-listchanges: changelogs for %s"
msgstr "apt-listchanges: %s 的改變紀錄"

#: ../apt-listchanges.py:151
#, fuzzy
#| msgid "didn't find any valid .deb archives"
msgid "Didn't find any valid .deb archives"
msgstr "沒有找到任何有效的 .deb 檔案"

#: ../apt-listchanges.py:168
#, python-format
msgid "%s: will be newly installed"
msgstr "%s：將被全新安裝"

#: ../apt-listchanges.py:195
#, python-format
msgid "%(pkg)s: Version %(version)s has already been seen"
msgstr "%(pkg)s：%(version)s 版本已經被看過了"

#: ../apt-listchanges.py:199
#, fuzzy, python-format
#| msgid "%(pkg)s: Version %(version)s has already been seen"
msgid ""
"%(pkg)s: Version %(version)s is lower than version of related packages "
"(%(maxversion)s)"
msgstr "%(pkg)s：%(version)s 版本已經被看過了"

#: ../apt-listchanges.py:271
#, python-format
msgid "Received signal %d, exiting"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:54
msgid "Aborting"
msgstr "放棄中"

#: ../apt-listchanges/apt_listchanges.py:59
#, fuzzy, python-format
msgid "Confirmation failed: %s"
msgstr "訊息備忘"

#: ../apt-listchanges/apt_listchanges.py:63
#, python-format
msgid "Mailing %(address)s: %(subject)s"
msgstr "寄送 %(address)s: %(subject)s"

#: ../apt-listchanges/apt_listchanges.py:82
#, fuzzy, python-format
#| msgid "database %s failed to load."
msgid "Failed to send mail to %(address)s: %(errmsg)s"
msgstr "讀取資料庫 %s 失敗"

#: ../apt-listchanges/apt_listchanges.py:91
#, fuzzy, python-format
msgid "The mail frontend needs an installed 'sendmail', using %s"
msgstr "mail 介面需要可用的 'sendmail'，改用分頁器"

#: ../apt-listchanges/apt_listchanges.py:97
#, fuzzy, python-format
msgid "The mail frontend needs an e-mail address to be configured, using %s"
msgstr "mail 介面需要可用的 'sendmail'，改用分頁器"

#: ../apt-listchanges/apt_listchanges.py:110
#, fuzzy
msgid "Available apt-listchanges frontends:"
msgstr "apt-listchanges: 消息"

#: ../apt-listchanges/apt_listchanges.py:112
msgid "Choose a frontend by entering its number: "
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:121
#: ../apt-listchanges/apt_listchanges.py:415
#, python-format
msgid "Error: %s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:123
#, fuzzy, python-format
msgid "Using default frontend: %s"
msgstr "未知的介面：%s"

#: ../apt-listchanges/apt_listchanges.py:166
#, python-format
msgid "$DISPLAY is not set, falling back to %(frontend)s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:186
#, fuzzy, python-format
#| msgid ""
#| "The gtk frontend needs a working python-gtk2 and python-glade2.\n"
#| "Those imports can not be found. Falling back to pager.\n"
#| "The error is: %s"
msgid ""
"The gtk frontend needs a working python3-gi,\n"
"but it cannot be loaded. Falling back to %(frontend)s.\n"
"The error is: %(errmsg)s"
msgstr ""
"使用 gtk 介面需要 python-gtk2 與 python-glade2\n"
"無法找到需要的函式庫。退回使用分頁器\n"
"錯誤：%s"

#: ../apt-listchanges/apt_listchanges.py:287
msgid "Do you want to continue? [Y/n] "
msgstr "您想繼續嗎？[Y/n]？"

#: ../apt-listchanges/apt_listchanges.py:300
#: ../apt-listchanges/apt_listchanges.py:326
#: ../apt-listchanges/apt_listchanges.py:334
msgid "Reading changelogs"
msgstr "讀取改變紀錄(changelogs)"

#: ../apt-listchanges/apt_listchanges.py:334
msgid "Done"
msgstr "完成"

#: ../apt-listchanges/apt_listchanges.py:363
#, python-format
msgid "Command %(cmd)s exited with status %(status)d"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:397
#, python-format
msgid "Found user: %(user)s, temporary directory: %(dir)s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:443
#, python-format
msgid "Error getting user from variable '%(envvar)s': %(errmsg)s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:449
msgid "Cannot find suitable user to drop root privileges"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:475
#, python-format
msgid ""
"None of the following directories is accessible by user %(user)s: %(dirs)s"
msgstr ""

#: ../apt-listchanges/apt_listchanges.py:505
msgid "press q to quit"
msgstr ""

#: ../apt-listchanges/ALCConfig.py:89
#, fuzzy, python-format
msgid "Unknown configuration file option: %s"
msgstr "訊息備忘"

#: ../apt-listchanges/ALCConfig.py:102
msgid "Usage: apt-listchanges [options] {--apt | filename.deb ...}\n"
msgstr "使用方法： apt-listchanges [options] {--apt | filename.deb ...}\n"

#: ../apt-listchanges/ALCConfig.py:108
#, fuzzy, python-format
#| msgid "Unknown option %s for %s.  Allowed are: %s."
msgid "Unknown argument %(arg)s for option %(opt)s.  Allowed are: %(allowed)s."
msgstr "%s 未知的選項 %s。可以使用的有： %s。"

#: ../apt-listchanges/ALCConfig.py:120
#, python-format
msgid "%(deb)s does not have '.deb' extension"
msgstr ""

#: ../apt-listchanges/ALCConfig.py:123
#, fuzzy, python-format
msgid "%(deb)s does not exist or is not a file"
msgstr "apt-listchanges: %s 的消息"

#: ../apt-listchanges/ALCConfig.py:126
#, fuzzy, python-format
msgid "%(deb)s is not readable"
msgstr "apt-listchanges: %s 的消息"

#: ../apt-listchanges/ALCConfig.py:217
msgid "--since=<version> and --show-all are mutually exclusive"
msgstr ""

#: ../apt-listchanges/ALCConfig.py:225
msgid "--since=<version> expects a path to exactly one .deb archive"
msgstr ""

#: ../apt-listchanges/DebianFiles.py:118 ../apt-listchanges/DebianFiles.py:127
#, python-format
msgid "Error processing '%(what)s': %(errmsg)s"
msgstr ""

#: ../apt-listchanges/DebianFiles.py:273
#, python-format
msgid "Calling %(cmd)s to retrieve changelog"
msgstr ""

#: ../apt-listchanges/DebianFiles.py:277
#, python-format
msgid ""
"Unable to retrieve changelog for package %(pkg)s; 'apt-get changelog' failed "
"with: %(errmsg)s"
msgstr ""

#: ../apt-listchanges/DebianFiles.py:283
#, python-format
msgid ""
"Unable to retrieve changelog for package %(pkg)s; could not run 'apt-get "
"changelog': %(errmsg)s"
msgstr ""

#: ../apt-listchanges/DebianFiles.py:322
#, python-format
msgid "Ignoring `%s' (seems to be a directory!)"
msgstr "忽略 `%s' (看來是個目錄！)"

#: ../apt-listchanges/AptListChangesGtk.py:42
#, fuzzy
#| msgid "apt-listchanges: Changelogs"
msgid "apt-listchanges: Reading changelogs"
msgstr "apt-listchanges: 改變紀錄"

#: ../apt-listchanges/AptListChangesGtk.py:43
#, fuzzy
msgid "Reading changelogs. Please wait."
msgstr "讀取改變紀錄(changelogs)"

#: ../apt-listchanges/AptListChangesGtk.py:75
msgid "Continue Installation?"
msgstr "繼續安裝程序？"

#: ../apt-listchanges/AptListChangesGtk.py:75
msgid "You can abort the installation if you select 'no'."
msgstr "你可以選擇 'no' 放棄安裝程序。"

#: ../apt-listchanges/ALCApt.py:59
msgid "APT pipeline messages:"
msgstr ""

#: ../apt-listchanges/ALCApt.py:66
msgid "Packages list:"
msgstr ""

#: ../apt-listchanges/ALCApt.py:74
#, fuzzy
#| msgid ""
#| "Wrong or missing VERSION from apt pipeline\n"
#| "(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)"
msgid ""
"APT_HOOK_INFO_FD environment variable is not defined\n"
"(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::InfoFD set to 20?)"
msgstr ""
"從 apt 傳來的訊息缺少版本號或是存在錯誤的版本號\n"
"(Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version 被設為 2 了嗎？)"

#: ../apt-listchanges/ALCApt.py:80
msgid "Invalid (non-numeric) value of APT_HOOK_INFO_FD environment variable"
msgstr ""

#: ../apt-listchanges/ALCApt.py:83
#, python-format
msgid "Will read apt pipeline messages from file descriptor %d"
msgstr ""

#: ../apt-listchanges/ALCApt.py:86
msgid ""
"Incorrect value (0) of APT_HOOK_INFO_FD environment variable.\n"
"If the warning persists after restart of the package manager (e.g. "
"aptitude),\n"
"please check if the /etc/apt/apt.conf.d/20listchanges file was properly "
"updated."
msgstr ""

#: ../apt-listchanges/ALCApt.py:91
#, fuzzy
#| msgid ""
#| "Wrong or missing VERSION from apt pipeline\n"
#| "(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)"
msgid ""
"APT_HOOK_INFO_FD environment variable is incorrectly defined\n"
"(Dpkg::Tools::Options::/usr/bin/apt-listchanges::InfoFD should be greater "
"than 2)."
msgstr ""
"從 apt 傳來的訊息缺少版本號或是存在錯誤的版本號\n"
"(Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version 被設為 2 了嗎？)"

#: ../apt-listchanges/ALCApt.py:97
#, python-format
msgid "Cannot read from file descriptor %(fd)d: %(errmsg)s"
msgstr ""

#: ../apt-listchanges/ALCApt.py:103
msgid ""
"Wrong or missing VERSION from apt pipeline\n"
"(is Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version set to 2?)"
msgstr ""
"從 apt 傳來的訊息缺少版本號或是存在錯誤的版本號\n"
"(Dpkg::Tools::Options::/usr/bin/apt-listchanges::Version 被設為 2 了嗎？)"

#: ../apt-listchanges/ALCLog.py:30 ../apt-listchanges/ALCLog.py:36
#, fuzzy, python-format
#| msgid "apt-listchanges: News"
msgid "apt-listchanges: %(msg)s"
msgstr "apt-listchanges: 消息"

#: ../apt-listchanges/ALCLog.py:33
#, fuzzy, python-format
#| msgid "apt-listchanges: Changelogs"
msgid "apt-listchanges warning: %(msg)s"
msgstr "apt-listchanges: 改變紀錄"

#: ../apt-listchanges/ALCSeenDb.py:56
msgid ""
"Path to the seen database is unknown.\n"
"Please either specify it with --save-seen option\n"
"or pass --profile=apt to have it read from the configuration file."
msgstr ""

#: ../apt-listchanges/ALCSeenDb.py:67
#, python-format
msgid "Database %(db)s does not end with %(ext)s"
msgstr ""

#: ../apt-listchanges/ALCSeenDb.py:76
#, fuzzy, python-format
#| msgid "database %s failed to load."
msgid "Database %(db)s failed to load: %(errmsg)s"
msgstr "讀取資料庫 %s 失敗"

#: ../apt-listchanges/apt-listchanges.ui:13
msgid "List the changes"
msgstr ""

#: ../apt-listchanges/apt-listchanges.ui:43
msgid ""
"The following changes are found in the packages you are about to install:"
msgstr ""

#~ msgid "The %s frontend is deprecated, using pager"
#~ msgstr "%s 介面已被廢除，改用分頁器"

#~ msgid "Confirmation failed, don't save seen state"
#~ msgstr "確認失敗，不除存狀態"

#~ msgid "Can't set locale; make sure $LC_* and $LANG are correct!\n"
#~ msgstr "無法設定 locale；請確定 $LC_* 與 $LANG 設定正確\n"
